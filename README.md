Welkom op de KASH-docs repo a.k.a. Project Capensis

KASH aka Project Bathyergidae
  * [kash-api (Project Hottentotus): Een RESTful API voor de andere clients](https://gitlab.com/thedjdoorn/kash-api)
  * [kash-docs (Project Glaber): Documentatie van de API en clients](https://gitlab.com/thedjdoorn/kash-docs)
  * <strong> kash-web (Project Capensis): Een webinterface
  * [kash-android (Project Suillus): Een Android client](https://gitlab.com/thedjdoorn/kash-android)
  * [kash-ios (Project Janetta): Een iOS client](https://gitlab.com/thedjdoorn/kash-ios)

  ___
  Op deze repo vind je documentatie in verschillende vormen, zoals
  * REST API Documentatie
  * Diagrammen
  * Usage Guides

  Enjoy!
